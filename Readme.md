## Files : 

- ``hosts``: Contains the actual hosts on which code needs to be deployed. 

- ``main.yml`` : the File that instructs where to deploy what. 

- ``Keys`` : Folder needs to have pem files to server for the deployment to happen. 

- Running this : ``ansible-playbook -i hosts main.yml -vvv``

- [Ansible Playbook Documentation](https://docs.ansible.com/ansible/latest/user_guide/playbooks.html)

- [Ansible Inventory Documentation](https://docs.ansible.com/ansible/latest/user_guide/intro_inventory.html)